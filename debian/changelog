nss-updatedb (10-7) unstable; urgency=medium

  * QA upload.
  * Set Debian QA Group as maintainer. (see #1076403)
  * debian/control: bumped Standards-Version to 4.7.0.
  * debian/copyright: updated some years.

 -- Fabio Augusto De Muzio Tobich <ftobich@debian.org>  Mon, 15 Jul 2024 15:38:19 -0300

nss-updatedb (10-6) unstable; urgency=medium

  * debian/control: bumped Standards-Version to 4.6.0.
  * debian/salsa-ci.yml: use Debian recipe for salsa CI.
  * debian/watch: updated to get the latest git commit since upstream do not
    set release versions.

 -- Fabio Augusto De Muzio Tobich <ftobich@debian.org>  Tue, 19 Oct 2021 10:32:29 -0300

nss-updatedb (10-5) unstable; urgency=medium

  * Upload to unstable.

 -- Fabio Augusto De Muzio Tobich <ftobich@debian.org>  Mon, 16 Aug 2021 13:53:31 -0300

nss-updatedb (10-4) experimental; urgency=medium

  * New maintainer. (Closes: #579616)
    Thanks to Guido Günther for your great work on this package.
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field and bumped level to 13.
  * debian/control:
      - Added Rules-Requires-Root: no.
      - Added Vcs-* fields to source stanza.
      - Bumped Standards-Version to 4.5.1.
      - Changed Homepage field to GitHub URL, old URL don't exist anymore.
      - Changed priority extra to optional.
      - Dependency on autotools-dev removed from Build-Depends field in source
        stanza, not necessary since DH 10.
  * debian/copyright:
      - Migrated to 1.0 format.
      - Updated all data.
  * debian/patches/:
      - 0001-Adjust-paths-to-debian-s-location-of-the-db-files.patch: updated
        header.
      - 0002-Update-config.-sub-guess.patch: updated header.
      - 0003-Fix-typo-in-manpage.patch: added, thanks Chris Kuehl.
        (Closes: #773927)
  * debian/rules:
      - Added DEB_BUILD_MAINT_OPTIONS variable to improve GCC
        hardening.
      - Migrated to debhelper reduced form.
  * debian/salsa-ci.yml: added to provide CI tests for Salsa.
  * debian/upstream/metadata: created.
  * debian/watch:
      - Bumped to version 4.
      - Changed URL to GitHub repository.
      - Removed template comments.

 -- Fabio Augusto De Muzio Tobich <ftobich@debian.org>  Sat, 29 May 2021 10:33:20 -0300

nss-updatedb (10-3) unstable; urgency=low

  * [5377b82] Use autotools-dev (Closes: #727474)

 -- Guido Günther <agx@sigxcpu.org>  Fri, 25 Oct 2013 17:48:14 +0200

nss-updatedb (10-2) unstable; urgency=low

  * Acknowledge NMU - thanks Jari!
  * [5b6f681] Import patches using gbp-pq
  * [8cf65c5] Drop config.{sub,guess} update

 -- Guido Günther <agx@sigxcpu.org>  Sat, 15 May 2010 15:08:05 +0200

nss-updatedb (10-1.1) unstable; urgency=low

  [ Jari Aalto ]
  * Non-maintainer upload.
    - Move to packaging format "3.0 (quilt)"
      (FTBFS minor; Closes: #482729).
  * debian/compat
    - Update to 7.
  * debian/control
    - (Build-Depends): remove quilt; obsolete. Update from
      libdb4.6-dev to libdb-dev >= 4.7 (Closes: #548485).
    - (Standards-Version): Update to 3.8.4 and debhelper 7.1.
  * debian/copyright
    - Point to GPL-2 and a minor layout update.
  * debian/rules
    - Delete obsolete quilt calls.
  * debian/rules
    - (install): Update dh_clean to dh_prep.
    - (clean): Fix lintian debian-rules-ignores-make-clean-error
      (Closes: #480953). Patch thanks to
      Miguel Angel Ruiz Manzano <mruiz@ubuntu.com>
  * debian/source/format
   - New file

 -- Jari Aalto <jari.aalto@cante.net>  Fri, 07 May 2010 12:10:35 +0300

nss-updatedb (10-1) unstable; urgency=low

  * drop return-zero-in-case-of-success.patch - applied upstream
  * drop manpage - added upstream

 -- Guido Guenther <agx@sigxcpu.org>  Wed, 23 Apr 2008 18:12:56 +0200

nss-updatedb (9-2) unstable; urgency=low

  * return zero in case of success  (Closes: #477276)
  * debian/copyright: add copyright year and holder
  * debian/rules: remove superflous comments
  * nss_updatedb.8: remove spurios ''' from manpage

 -- Guido Guenther <agx@sigxcpu.org>  Tue, 22 Apr 2008 12:00:38 +0200

nss-updatedb (9-1) unstable; urgency=low

  * New Upstream Version (Closes: #431862)
  * point to correct paths in README file (Closes: #458846)
  * add Homepage:
  * bump standards version
  * build depend on libdb4.6-dev

 -- Guido Guenther <agx@sigxcpu.org>  Fri, 22 Feb 2008 11:40:01 +0100

nss-updatedb (8-2) unstable; urgency=low

  * fix watch file - based on a patch by Thomas Gleissner, thanks!
    (Closes: #449875)

 -- Guido Guenther <agx@sigxcpu.org>  Wed, 28 Nov 2007 09:14:52 +0100

nss-updatedb (8-1) unstable; urgency=low

  * New upstream version
  * Drop 02_move-db.diff - applied upstream

 -- Guido Guenther <agx@sigxcpu.org>  Wed, 31 Oct 2007 12:47:21 +0100

nss-updatedb (7-3) unstable; urgency=low

  * libdb4.3-dev reverted the change that made linking against libdb4.3-dev
    necessary, so drop that from the linker flags

 -- Guido Guenther <agx@sigxcpu.org>  Sat, 22 Sep 2007 17:59:09 +0200

nss-updatedb (7-2) experimental; urgency=low

  * link against pthread (Closes: #441483)
  * fill a temporary database, then replace the running one (Closes: #431862)

 -- Guido Guenther <agx@sigxcpu.org>  Mon, 10 Sep 2007 18:59:44 +0200

nss-updatedb (7-1) unstable; urgency=low

  * New upstream version
  * dropped patches:
        10_segv-on-dbopen: applied upstream

 -- Guido Guenther <agx@sigxcpu.org>  Tue, 14 Nov 2006 09:30:41 +0100

nss-updatedb (6-2) unstable; urgency=low

  * new patch:
        10_segv-on-dbopen: don't try to close the db environment when it can't
        be even opened

 -- Guido Guenther <agx@sigxcpu.org>  Mon, 13 Nov 2006 11:41:02 +0100

nss-updatedb (6-1) unstable; urgency=low

  * new upstream version
  * drop patches (applied upstream):
         10_num-should-be-unsigned
         12_remove-double-truncate

 -- Guido Guenther <agx@sigxcpu.org>  Thu, 18 May 2006 02:27:02 -0500

nss-updatedb (5-1) unstable; urgency=low

  * new upstream version
  * new patches:
         10_num-should-be-unsigned: the num argument to db_truncate should be
         unsigned, fixes a compiler warning
         12_remove-double-truncate: no need to truncate the db twice
  * drop patches:
         10_nss-updatedb-fix-db4-truncate
         11_closedb
         12_db-autocommit
    applied upstream.
  * bump standards version to 3.7.2
  * update fsf copyright address

 -- Guido Guenther <agx@sigxcpu.org>  Wed, 17 May 2006 01:01:20 -0500

nss-updatedb (4-4) unstable; urgency=low

  * 10_nss-updatedb-fix-db4-truncate: num should be unsigned
  * 11_closedb: properly close the database before closing the environment
  * 12_db-autocommit: use DB_AUTO_COMMIT with db->open so aborting the
    transaction works properly. Thanks to Steffen Kolbe for providing a
    testmachine and testing.
  * Bump standars version

 -- Guido Guenther <agx@sigxcpu.org>  Thu,  4 May 2006 10:08:53 +0200

nss-updatedb (4-3) unstable; urgency=low

  * debian/rules: make sure ./configure is executable, remove config.log

 -- Guido Guenther <agx@debian.org>  Tue, 22 Nov 2005 15:48:00 +0100

nss-updatedb (4-2) unstable; urgency=low

  * patches/10_nss-updatedb-fix-db4-truncate: properly truncate the
    database in the db4 case

 -- Guido Guenther <agx@debian.org>  Tue, 22 Nov 2005 15:46:36 +0100

nss-updatedb (4-1) unstable; urgency=low

  * new upstream version
  * patches/02_fix-db3.dpatch dropped, applied upstream
  * patches/03_fix-nss-enum.dpatch dropped, applied upstream
  * patches/04_let-main-return-0.dpatch: dropped, fixed upstream

 -- Guido Guenther <agx@debian.org>  Wed,  3 Aug 2005 12:07:12 +0200

nss-updatedb (3-5) unstable; urgency=low

  * compile against libdb4.3-dev
  * bump standards version to 3.6.2

 -- Guido Guenther <agx@debian.org>  Mon,  1 Aug 2005 17:26:12 +0200

nss-updatedb (3-4) unstable; urgency=low

  * update package description, thanks to Thomas Hood (closes: #314918)
  * add manpage (closes: #314917)

 -- Guido Guenther <agx@debian.org>  Wed, 13 Jul 2005 22:04:47 +0300

nss-updatedb (3-3) unstable; urgency=low

  * change priority to extra

 -- Guido Guenther <agx@debian.org>  Wed, 20 Apr 2005 20:21:57 +0200

nss-updatedb (3-2) unstable; urgency=low

  * let main() return zero instead of NSS_UPDATE_SUCCESS

 -- Guido Guenther <agx@debian.org>  Wed, 20 Apr 2005 15:53:55 +0200

nss-updatedb (3-1) unstable; urgency=low

  * Initial Release (Closes: #303208)

 -- Guido Guenther <agx@debian.org>  Tue,  5 Apr 2005 18:24:09 +0200
